/**
 * This is a generated file. Do not edit or your changes will be lost
 */

@interface CoMobiledatasystemsTibeaconModuleAssets : NSObject
{
}
- (NSData*) moduleAsset;
- (NSData*) resolveModuleAsset:(NSString*)path;

@end
