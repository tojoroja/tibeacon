/**
 * Your Copyright Here
 *
 * Appcelerator Titanium is Copyright (c) 2009-2010 by Appcelerator, Inc.
 * and licensed under the Apache Public License (version 2)
 */
#import "TiModule.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface CoMobiledatasystemsTibeaconModule : TiModule<CLLocationManagerDelegate> 
{
    KrollCallback   *successCallback;
    KrollCallback   *errorCallback;
    KrollCallback   *rangedCallback;
    KrollCallback   *regionCallback;
    KrollCallback   *changeCallback;
}




@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic, strong) NSMutableDictionary *previousBeacons;
@property (nonatomic, strong) NSMutableDictionary *beaconRegions;
@property (nonatomic, strong) NSMutableDictionary *beaconRangingOutsideRegion;

@end
