/**
 * Your Copyright Here
 *
 * Appcelerator Titanium is Copyright (c) 2009-2010 by Appcelerator, Inc.
 * and licensed under the Apache Public License (version 2)
 */
#import "CoMobiledatasystemsTibeaconModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"

@implementation CoMobiledatasystemsTibeaconModule

#pragma mark Internal

// this is generated for your module, please do not change it
-(id)moduleGUID
{
	return @"2ecb69fc-dd61-423d-bf99-02b18a1457b2";
}

// this is generated for your module, please do not change it
-(NSString*)moduleId
{
	return @"co.mobiledatasystems.tibeacon";
}

#pragma mark Lifecycle

-(void)startup
{
	// this method is called when the module is first loaded
	// you *must* call the superclass
	[super startup];
    
	self.beaconRegions = [[NSMutableDictionary alloc] init];
    self.previousBeacons = [[NSMutableDictionary alloc] init];
    self.beaconRangingOutsideRegion = [[NSMutableDictionary alloc] init];
	NSLog(@"[INFO] %@ loaded",self);
}

-(void)shutdown:(id)sender
{
	// this method is called when the module is being unloaded
	// typically this is during shutdown. make sure you don't do too
	// much processing here or the app will be quit forceably
	
	// you *must* call the superclass
    if(_beaconRegions != nil){
        NSLog(@"[INFO] stopping monitoring beaconRegions", nil);
        [self stopAllBeacons:nil];
        RELEASE_TO_NIL(self.beaconRegions);
        RELEASE_TO_NIL(self.beaconRangingOutsideRegion);
        RELEASE_TO_NIL(self.previousBeacons);
    }
	[super shutdown:sender];
}

#pragma mark Cleanup

-(void)dealloc
{
	// release any resources that have been retained by the module
	[super dealloc];
}

#pragma mark Internal Memory Management

-(void)didReceiveMemoryWarning:(NSNotification*)notification
{
	// optionally release any resources that can be dynamically
	// reloaded once memory is available - such as caches
	[super didReceiveMemoryWarning:notification];
}

#pragma mark Listener Notifications

-(void)_listenerAdded:(NSString *)type count:(int)count
{
	if (count == 1 && [type isEqualToString:@"my_event"])
	{
		// the first (of potentially many) listener is being added
		// for event named 'my_event'
	}
}

-(void)_listenerRemoved:(NSString *)type count:(int)count
{
	if (count == 0 && [type isEqualToString:@"my_event"])
	{
		// the last listener called for event named 'my_event' has
		// been removed, we can optionally clean up any resources
		// since no body is listening at this point for that event
	}
}



#pragma Public APIs
-(void)stopAllBeacons:(id)args
{
    if (self.locationManager.monitoredRegions.count == 0) {
        NSLog(@"[INFO] Didn't turn off monitoring: Monitoring already off.");
    } else {
        NSEnumerator *enumerator = [self.beaconRegions keyEnumerator];
        id key;
        while (key = [enumerator nextObject]) {
            
            CLBeaconRegion *beaconRegion = [self.beaconRegions objectForKey:key];
            
            if (beaconRegion != nil) {
                [self.locationManager stopMonitoringForRegion:beaconRegion];
                [self.locationManager stopRangingBeaconsInRegion:beaconRegion];
            }
            else {
                NSLog(@"[ERROR] stopMonitoringAllRegions() Unable to find beaconRegion for %@.", beaconRegion);
            }
        }
        NSLog(@"[INFO] Turned off monitoring in ALL regions belonging to this app.");
    }
    [self.beaconRegions removeAllObjects];
    [self.beaconRangingOutsideRegion removeAllObjects];
    [self.previousBeacons removeAllObjects];
}

-(void)initializeBeaconMonitoring:(id)args
{
    ENSURE_UI_THREAD_1_ARG(args);
    ENSURE_SINGLE_ARG(args,NSDictionary);
    
    NSLog(@"[INFO] initializeBeaconMonitoring 1.0", nil);
    
    if(_beaconRegions != nil){
        //NSLog(@"[INFO] stopping monitoring beaconRegions", nil);
        [self stopAllBeacons:nil];
    }

    //if the globals are already initialised because this function has already been called, clear everything down
    RELEASE_TO_NIL(successCallback);
    RELEASE_TO_NIL(errorCallback);
    RELEASE_TO_NIL(rangedCallback);
    RELEASE_TO_NIL(regionCallback);
    RELEASE_TO_NIL(changeCallback);
    
    RELEASE_TO_NIL(self.locationManager);

    //first the success and error event handlers
    id success                          = [args objectForKey:@"success"];
    id ranged                           = [args objectForKey:@"ranged"];
    id region                           = [args objectForKey:@"region"];
    id change                            = [args objectForKey:@"change"];
    id error                            = [args objectForKey:@"error"];
    
    successCallback                     = [success retain];
    rangedCallback                      = [ranged retain];
    regionCallback                      = [region retain];
    changeCallback                      = [change retain];
    errorCallback                       = [error retain];
    
    
    if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]])
    {
        
        // Initialize location manager and set ourselves as the delegate
        self.locationManager            = [[CLLocationManager alloc] init];
        self.locationManager.delegate   = self;
        
        //create the event object passed back by the event handlet
        NSDictionary *successEvent      = [NSDictionary dictionaryWithObjectsAndKeys:
                                           NUMBOOL(YES), @"success",
                                           NUMBOOL(NO), @"error",
                                           nil];
        
        [self _fireEventToListener:@"success" withObject:successEvent listener:successCallback thisObject:nil];
        
    } else {
        
        //create the event object passed back by the event handlet
        NSDictionary *errorEvent        = [NSDictionary dictionaryWithObjectsAndKeys:
                                           NUMBOOL(NO), @"success",
                                           NUMBOOL(YES), @"error",
                                           @"beacons not supported", @"message",
                                           nil];
        
        [self _fireEventToListener:@"error" withObject:errorEvent listener:errorCallback thisObject:nil];
    }
}


-(void)startMonitoringBeaconRegion:(id)args
{
    ENSURE_UI_THREAD_1_ARG(args);
    ENSURE_SINGLE_ARG(args,NSDictionary);
    
    
    if([self.beaconRegions count] > 5){
        //create the event object passed back by the event handlet
        NSDictionary *errorEvent        = [NSDictionary dictionaryWithObjectsAndKeys:
                                           NUMBOOL(NO), @"success",
                                           NUMBOOL(YES), @"error",
                                           @"maximum of 5 regions allowed", @"message",
                                           nil];
        
        [self _fireEventToListener:@"error" withObject:errorEvent listener:errorCallback thisObject:nil];
    } else {
        id TIuuid                           = [args objectForKey:@"uuid"];
        id TIidentifier                     = [args objectForKey:@"identifier"];
        id TINotifyEntryStateOnDisplay      = [args objectForKey:@"notifyEntryStateOnDisplay"];
        id TIkeepRanging                    = [args objectForKey:@"keepRanging"];
        id TIMajor                          = [args objectForKey:@"major"];
        id TIMinor                          = [args objectForKey:@"minor"];
        
        NSString *uuidS                     = [TiUtils stringValue:TIuuid];
        NSString *identifier                = [TiUtils stringValue:TIidentifier];
        BOOL notifyEntryStateOnDisplay   = [TiUtils boolValue:TINotifyEntryStateOnDisplay def:NO]; //default to allow notifications in background
        NSNumber *keepRanging               = [NSNumber numberWithBool:[TiUtils boolValue:TIkeepRanging def:YES]]; //default to continuously monitor for ranging events
        
        //NSLog(@"[INFO] uuid: %@", uuidS);
        //NSLog(@"[INFO] identifier: %@", identifier);
        
        CLBeaconRegion *beaconRegion;
        NSUUID *uuid                        = [[NSUUID alloc] initWithUUIDString:uuidS];
        
        if(TIMajor == nil)
        {
            // Setup a new region with that UUID
            beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:identifier];
            
        } else {
            // Setup a new region with that UUID and major number
            NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
            unsigned short major = [[formatter numberFromString:[TiUtils stringValue:TIMajor]] unsignedShortValue];
            //NSLog(@"[INFO] major: %hu", major);
            
            if(TIMinor == nil)
            {
                beaconRegion =[[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major identifier:identifier];
            } else {
                // Setup a new region with that UUID, major and minor number
                unsigned short minor     = [[formatter numberFromString:[TiUtils stringValue:TIMinor]] unsignedShortValue];
                //NSLog(@"[INFO] minor: %hu", minor);
                beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:major minor:minor identifier:identifier];
            }
            [formatter release];
        }
        [uuid release];
        
        //NSLog(@"[INFO] adding beacon %@", beaconRegion.identifier);
        
        beaconRegion.notifyEntryStateOnDisplay = notifyEntryStateOnDisplay;
        beaconRegion.notifyOnEntry = true;
        beaconRegion.notifyOnExit = true;
        
        
        [self.beaconRegions setObject:beaconRegion forKey:beaconRegion.identifier];
        [self.beaconRangingOutsideRegion setObject:keepRanging forKey:beaconRegion.identifier];
        
        [self.locationManager startMonitoringForRegion:beaconRegion];
        [self.locationManager startRangingBeaconsInRegion:beaconRegion];
    }
    
}

-(void)stopMonitoringBeaconRegion:(id)args
{
    ENSURE_UI_THREAD_1_ARG(args);
    ENSURE_SINGLE_ARG(args,NSDictionary);
    
    //NSLog(@"[INFO] removeBeaconRegion 0.1", nil);
    
    id TIidentifier = [args objectForKey:@"identifier"];
    NSString *identifier = [TiUtils stringValue:TIidentifier];
    
    [self.locationManager stopRangingBeaconsInRegion:[self.beaconRegions objectForKey:identifier]];
    [self.locationManager stopMonitoringForRegion:[self.beaconRegions objectForKey:identifier]];
    
    [self.beaconRegions removeObjectForKey:identifier];
    [self.beaconRangingOutsideRegion removeObjectForKey:identifier];
    [self.previousBeacons removeObjectForKey:identifier];

    NSLog(@"[INFO] Turned off beacon %@.", identifier);
}

///////////////////

- (void)sendLocalNotification:(id) args
{
    NSLog(@"[INFO] sendLocalNotification", nil);
    
    ENSURE_UI_THREAD_1_ARG(args);
    ENSURE_SINGLE_ARG(args,NSDictionary);
    
    NSDate *alertTime = [[NSDate date] dateByAddingTimeInterval:3];
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc] init];
    
    if(notifyAlarm){
        NSLog(@"[INFO] notifyAlarm instantiated", nil);
        
        id TINotificationMessage = [args objectForKey:@"message"];
        
        if(TINotificationMessage != nil){
            
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;
            notifyAlarm.alertBody = [TiUtils stringValue:TINotificationMessage];
            
            id TINotificationSound = [args objectForKey:@"sound"];
            if(TINotificationSound != nil){
                notifyAlarm.soundName = [TiUtils stringValue:TINotificationSound];
            }
            
            NSLog(@"[INFO] scheduling alarm", nil);
            [app scheduleLocalNotification:notifyAlarm];
            
        }
    }
}


#pragma CLLocationService delegates

- (void) locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    //NSLog(@"[INFO] Did start monitoring region: %@", region.identifier);
    [self.locationManager requestStateForRegion:region];
}


-(void) locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    
    CLBeaconRegion *beaconRegion = [self.beaconRegions objectForKey:region.identifier];
    if(beaconRegion){
        if(state == CLRegionStateInside){
            //Start Ranging - we need to do this because the didEnterRegion is not fired if we are already in the region when we begin monitoring...
            //NSLog(@"starting ranging region: ", beaconRegion.identifier);
            [self.locationManager startRangingBeaconsInRegion:beaconRegion];
        } else {
            if (state == CLRegionStateOutside){
                if(![[self.beaconRangingOutsideRegion objectForKey:region.identifier] boolValue]){
                    //NSLog(@"stopping ranging in region: ",region.identifier);
                    [self.locationManager stopRangingBeaconsInRegion:beaconRegion];
                    [self.locationManager startMonitoringForRegion:beaconRegion];
                }
            }
        }
    }
}


- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    NSLog(@"identifier %@", region.identifier);
    NSDictionary *event = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"entered",@"status",
                            region.identifier,@"identifier",
                            nil];
        
    CLBeaconRegion *beaconRegion = [self.beaconRegions objectForKey:region.identifier];
    if(beaconRegion){
        [self.locationManager startRangingBeaconsInRegion:beaconRegion];
            
        [self _fireEventToListener:@"region" withObject:event listener:regionCallback thisObject:nil];
        NSLog(@"[INFO] entered region", nil);
    }
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    //we are not in the region
    //create the event object passed back by the event handler
    NSDictionary *event = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"exited",@"status",
                            region.identifier,@"identifier",
                            nil];
        
    //turn off ranging if we have set keepRanging to NO when we exit the region
    CLBeaconRegion *beaconRegion = [self.beaconRegions objectForKey:region.identifier];
    if(beaconRegion){
        if(![[self.beaconRangingOutsideRegion objectForKey:region.identifier] boolValue]){
            [self.locationManager stopRangingBeaconsInRegion:beaconRegion];
            [self.locationManager startMonitoringForRegion:beaconRegion];
                
            [self _fireEventToListener:@"region" withObject:event listener:regionCallback thisObject:nil];
            NSLog(@"[INFO] exited region", nil);
        }
    }
}

-(NSString*) getProximity: (CLProximity)proximityReading
{
    NSString *proximity;
    
    switch (proximityReading)
    {
        case CLProximityUnknown:
            proximity = @"Unknown";
            break;
        case CLProximityImmediate:
            proximity = @"Immediate";
            break;
        case CLProximityNear:
            proximity = @"Near";
            break;
        case CLProximityFar:
            proximity = @"Far";
            break;
            
        default:
            proximity = @"Unknown";
            break;
    }
    return proximity;
}

-(void)alertIfNearestBeaconChanged: (CLBeacon*)nearestBeacon inRegion:(CLBeaconRegion*)region
{
    CLBeacon *previousBeacon = [self.previousBeacons objectForKey:region.identifier];
    //NSLog(@"nearestBeacon- major: %@ minor: %@", nearestBeacon.major, nearestBeacon.minor);
    //NSLog(@"previousBeacon- major: %@ minor: %@", previousBeacon.major, previousBeacon.minor);

    BOOL beaconChanged = NO;
    if(previousBeacon)
    {
            if([previousBeacon.major intValue] == [nearestBeacon.major intValue] &&
               [previousBeacon.minor intValue] == [nearestBeacon.minor intValue]){
                //NSLog(@"detected beacon has not changed", nil);
                beaconChanged = NO;
        } else {
            //NSLog(@"detected beacon change", nil);
            beaconChanged = YES; //the nearest beacon has changed
        }
    } else
    {
        //NSLog(@"detected no previous beacon", nil);
        beaconChanged = YES; //we have never yet allocated a previous beacon...
    }
  
    if(beaconChanged){
        //update the previous beacon to this one
        [self.previousBeacons setObject:nearestBeacon forKey:region.identifier];
        
        NSDictionary *changeEvent = [NSDictionary dictionaryWithObjectsAndKeys:
                                     NUMBOOL(YES),@"success",
                                     [[nearestBeacon proximityUUID] UUIDString], @"uuid",
                                     [NSString stringWithFormat:@"%@", [nearestBeacon major]], @"major",
                                     [NSString stringWithFormat:@"%@", [nearestBeacon minor]], @"minor",
                                     [NSString stringWithFormat:@"%f", [nearestBeacon accuracy]], @"accuracy",
                                     [NSString stringWithFormat:@"%ld", (long)[nearestBeacon rssi]], @"rssi",
                                     [self getProximity: [nearestBeacon proximity]], @"proximity",
                                     region.identifier,@"identifier",
                                     nil];
        
        [self _fireEventToListener:@"change" withObject:changeEvent listener:changeCallback thisObject:nil];
    }
}


-(void)locationManager:(CLLocationManager*)manager didRangeBeacons:(NSArray*)beacons inRegion:(CLBeaconRegion*)region
{
    //NSLog(@"didRangeBeacons", nil);
    
    CLBeacon *nearestBeacon;
    
    //NSLog(@"previousBeacon: %@", previousBeacon.minor);
    
    if ([beacons count] > 0) {
        // Beacon found! - create an array to hold beacon data in
        NSMutableArray *TIBeacons = [[NSMutableArray alloc] initWithCapacity:[beacons count]];
        
        nearestBeacon = [beacons objectAtIndex:0]; //the first beacon in the list is the nearest until we check...
        
        for(int i=0;i<[beacons count]; i++){
            
            CLBeacon *current = [beacons objectAtIndex:i];
            
            //now check to see if this beacon is nearer...
            if(current.accuracy < nearestBeacon.accuracy){
                nearestBeacon = current;
            }
            
            //create the beacon key/value pairs for the current beacon
            NSDictionary *beacon = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSString stringWithFormat:@"%i", i], @"id",
                                    [[current proximityUUID] UUIDString], @"uuid",
                                    [NSString stringWithFormat:@"%@", [current major]], @"major",
                                    [NSString stringWithFormat:@"%@", [current minor]], @"minor",
                                    [NSString stringWithFormat:@"%f", [current accuracy]], @"accuracy",
                                    [NSString stringWithFormat:@"%ld", (long)[current rssi]], @"rssi",
                                    [self getProximity: [current proximity]], @"proximity",
                                    region.identifier,@"identifier",
                                    nil];
            
            //add the beacon data to the beacon array
            [TIBeacons addObject: beacon];
            
        }
        //end of loop
        
        //create the event object passed back by the event handler
        NSDictionary *event = [NSDictionary dictionaryWithObjectsAndKeys:
                               NUMBOOL(YES),@"success",
                               [NSString stringWithFormat:@"%ld", (long)[beacons count]], @"beaconCount",
                               region.identifier,@"identifier",
                               TIBeacons, @"beacons",
                               nil];
        
        [self _fireEventToListener:@"ranged" withObject:event listener:rangedCallback thisObject:nil];
        
        //finally, clear down the array we used
        RELEASE_TO_NIL(TIBeacons);
        
        //and see if we need to send a beacon changed event as well
        [self alertIfNearestBeaconChanged: nearestBeacon inRegion:region];
    }
    else
    {
        //the beacon count returned was 0, i.e. no beacons found
        NSDictionary *event = [NSDictionary dictionaryWithObjectsAndKeys:
                               NUMBOOL(YES),@"success",
                               0, @"beaconCount",
                               region.identifier,@"identifier",
                               nil];
        
        [self _fireEventToListener:@"ranged" withObject:event listener:rangedCallback thisObject:nil];
        
        CLBeacon *previousBeacon = [self.previousBeacons objectForKey:region.identifier];
        if(previousBeacon){
            //there was a beacon previously; but now there are non in range...
            [self.previousBeacons removeObjectForKey:region.identifier];
            
            //create the event object passed back by the event handlet
            NSDictionary *changeEvent = [NSDictionary dictionaryWithObjectsAndKeys:
                                   NUMBOOL(YES),@"success",
                                   @"-1", @"uuid",
                                    @"-1",@"major",
                                    @"-1",@"minor",
                                   region.identifier,@"identifier",
                                   nil];
            
            [self _fireEventToListener:@"change" withObject:changeEvent listener:changeCallback thisObject:nil];
        }
    }
}
@end